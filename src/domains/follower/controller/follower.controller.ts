import { Request, Response, Router } from 'express';
import HttpStatus from 'http-status';
import "express-async-errors";

import { db } from '@utils';

import { FollowerRepositoryImpl } from '../repository';
import { FollowerService, FollowerServiceImpl } from '../service';

export const followerRouter = Router();

// Use dependency injection
const service: FollowerService = new FollowerServiceImpl(new FollowerRepositoryImpl(db));

followerRouter.post('/follow/:userId ', async (req: Request, res: Response) =>{
  const { userId } = res.locals.context;
  const { userId: otherUserId } = req.params;

  const response = await service.createFollower(userId, otherUserId);

  return res.status(HttpStatus.OK).json(response);
});


// userRouter.get('/', async (req: Request, res: Response) => {
//   const { userId } = res.locals.context;
//   const { limit, skip } = req.query as Record<string, string>;

//   const users = await service.getUserRecommendations(userId, { limit: Number(limit), skip: Number(skip) });

//   return res.status(HttpStatus.OK).json(users);
// });

// userRouter.get('/me', async (req: Request, res: Response) => {
//   const { userId } = res.locals.context;

//   const user = await service.getUser(userId);

//   return res.status(HttpStatus.OK).json(user);
// });

// userRouter.get('/:userId', async (req: Request, res: Response) => {
//   const { userId: otherUserId } = req.params;

//   const user = await service.getUser(otherUserId);

//   return res.status(HttpStatus.OK).json(user);
// });

// userRouter.delete('/', async (req: Request, res: Response) => {
//   const { userId } = res.locals.context;

//   await service.deleteUser(userId);

//   return res.status(HttpStatus.OK);
// });
